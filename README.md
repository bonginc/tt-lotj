# TT LOTJ

This is a serie of scripts customizing Tintin++ MUD client.

Although some parts can be reused easilly whatever the MUD server (particularly widgets like scrollable text buffers, tabbed panels), this script contains several action/events specific to Legends of the Jedi MUD.

# Links

- [Legends of the Jedi MUD](https://www.legendsofthejedi.com/)
- [Tintin++ MUD client](https://tintin.mudhalla.net/)

# Screenshot

![tt-lotj screenshot](lotj-tt.gif)

#nop TABBED WINDOWS ---------------------------------------------------------;

/*
    We want to be able to spawn multiple tabbed windows.

    tw_data is a list of window objects

    A window (e.g. communications, tools) is :
        - window coords
        - a list of tabs
        
    a tab is :
        - a title
        - a list of messages (timestamped or not)
        - unread msg count
        - current page

    Word wrapping is managed by buffers.

    TODO:
        wrap check doesn't ignore <color> text width
*/

#nop var {tw_data} { };

#alias tw_create_window {

    #foreach $tw_data {window} {
        #if { "$window[id]" == "%1" } {
            #showme {tw_create_window : '%1' already exists};
            #return;
        }
    };

    #nop The dimension square is stored twice;
    #nop One is the initial user square, which may contain negative values;
    #nop One is the translated in absolute values one;
    #list {tw_data} {insert} {-1} {
        {index} -1
        {title} {%1}
        {tabs} { }
        {active} -1
        {user_top_row} {%2}
        {user_first_col} {%3}
        {user_end_row} {%4}
        {user_end_col} {%5}
        {top_row} {%2}
        {first_col} {%3}
        {end_row} {%4}
        {end_col} {%5}
        {height} -1
        {width} -1
        {tab_size} 15
        {tab_height} 2
    };

    #list {tw_data} {size} {win_count};
    #math {tw_data[-1][index]} { $win_count };
    tw_resize -1;
};

#alias tw_resize_all {
    #local {tw_idx} 1;
    #list {tw_data} {size} {tw_win_count};
    #while { $tw_idx <= $tw_win_count } {
        tw_resize $tw_idx;
        tw_draw_tabs $tw_idx;
        tw_draw_buffer $tw_idx;
        #math {tw_idx} {$tw_idx + 1};
    };
};

#alias tw_resize {
    #nop in case of resize;
    #line substitute {VARIABLES} {
        #unbutton { $tw_data[%1][top_row] $tw_data[%1][first_col] $tw_data[%1][end_row] $tw_data[%1][end_col] SCROLLED MOUSE WHEEL UP};
        #unbutton { $tw_data[%1][top_row] $tw_data[%1][first_col] $tw_data[%1][end_row] $tw_data[%1][end_col] SCROLLED MOUSE WHEEL DOWN};
    };
    tw_draw_tabs %1 {remove_buttons};

    #screen get rows screen_rows;
    #screen get cols screen_cols;
    #if { $tw_data[%1][user_top_row] < 0 } {
        #math {tw_data[%1][top_row]} { $screen_rows + $tw_data[%1][user_top_row] };
    } {
        #var {tw_data[%1][top_row]} {$tw_data[%1][user_top_row]};
    };
    #if { $tw_data[%1][user_first_col] < 0 } {
        #math {tw_data[%1][first_col]} { $screen_cols + $tw_data[%1][user_first_col] +1 };
    } {
        #var {tw_data[%1][first_col]} {$tw_data[%1][user_first_col]};
    };
    #if { $tw_data[%1][user_end_row] < 0 } {
        #math {tw_data[%1][end_row]} { $screen_rows + $tw_data[%1][user_end_row] };
    } {
        #var {tw_data[%1][end_row]} {$tw_data[%1][user_end_row]};
    };
    #if { $tw_data[%1][user_end_col] < 0 } {
        #math {tw_data[%1][end_col]} { $screen_cols + $tw_data[%1][user_end_col] +1 };
    } {
        #var {tw_data[%1][end_col]} {$tw_data[%1][user_end_col]};
    };

    #line substitute {VARIABLES} {
        #button { $tw_data[%1][top_row] $tw_data[%1][first_col] $tw_data[%1][end_row] $tw_data[%1][end_col] SCROLLED MOUSE WHEEL UP} {
            tw_offset_up $tw_data[%1][index] 1;
        };
        #button { $tw_data[%1][top_row] $tw_data[%1][first_col] $tw_data[%1][end_row] $tw_data[%1][end_col] SCROLLED MOUSE WHEEL DOWN} {
            tw_offset_down $tw_data[%1][index] 1;
        };
    };


    tw_draw_tabs %1 {init_buttons};

    #nop Buffer dimension evals;
    #local {height_offset} 3;
    #local {width_offset} 4;
    #math {tw_data[%1][height]} { $tw_data[%1][end_row] - $tw_data[%1][top_row] - $height_offset };
    #math {tw_data[%1][width]} { $tw_data[%1][end_col] - $tw_data[%1][first_col] - $width_offset };
    #unlocal {height_offset};
    #unlocal {width_offset};
};

#alias tw_create_tab {
    #foreach $tw_data[%1][tabs] {tab} {
        #if { "$tab[title]" == "%2" } {
            #showme {tw_create_tab : '%2' already exists for window '$tw_data[%1][title]'};
            #return;
        }
    };
    #list {tw_data[%1][tabs]} {insert} {-1} { 
        {index} -1
        {title} {%2}
        {unread} 0
        {data} { }
        {offset} 0
        {use_timestamps} {%3}
    };

    #list {tw_data[%1][tabs]} {size} {tab_count};
    #math {tw_data[-1][tabs][-1][index]} { $tab_count };
    tw_switch_tab %1 $tab_count;
};

#alias {tw_record} {
    #local {input_line} {};
    #if { $tw_data[%1][tabs][%2][use_timestamps] == 1 } {
        #format {time} {%t} {%H:%M};
        #cat {input_line} {<acf>$time %3<099>};
    } {
        #cat {input_line} {%3<099>};
    };
    
    #nop showme {input: '{$input_line}'};

    #nop We split lines larger than buffer width in multiple lines;
    #nop We manage word wrap as well;
    #list {tokens} {tokenize} {$input_line};
    #list {tokens} {size} {line_width};

    #local {buf_width} {$tw_data[%1][width]};
    #math {colored_buf_width} {$buf_width};
    #var {last_color} {};
    
    #if { $line_width > $colored_buf_width } {
        #local {j} 1;
        #local {k} 1;
        #var {tmp_line} {};
        #var {color_size} {};

        #var {printed_colors_offset} 0;

        #while { $j <= $line_width } {
            #nop showme {'$tokens[$j]' $j/$line_width, $k/$colored_buf_width };

            #nop word wrapping : if we're currently on a space, and there's no space anymore until end of line;
            #nop we do break line;

            #nop font colors (e.g. <acf>) don't count in line length !;
            #if { "$tokens[$j]" == "<" } {
                #var {tmp_color} {<};
                #math {x} {$j+1};
                #while { $x <= $line_width} {
                    #if { "$tokens[$x]" == ">" } {
                        #cat {tmp_color} {>};
                        #break;
                    };
                    #regexp {$tokens[$x]} {{[0-9a-f]}} {
                        #nop showme {   - color data : '$tokens[$x]'};
                        #cat {tmp_color} {$tokens[$x]};
                    } {
                        #nop showme {> fail : '$tokens[$x]'};
                        #var {tmp_color} {};
                    };
                    #if { "$tmp_color" == "" } {
                        #break;
                    };
                    #math {x} {$x + 1};
                };
                #unlocal {x};
           
                #if { "$tmp_color" != "" } {
                    #var {last_color} {$tmp_color};
                    #list {color_tokens} {tokenize} {$last_color};
                    #list {color_tokens} {size} {color_size};
                    #math {printed_colors_offset} {$printed_colors_offset + $color_size};
                    #math {colored_buf_width} {$buf_width + $printed_colors_offset};
                    #nop showme {Last known color : $color_tokens };
                };
                #unlocal {tmp_color};
            };
            #nop term font colors like \e[39mMYCOLOR don't count in line length too !;
            #if { "$tokens[$j]" == "\e" } {
                #nop showme {scanning TERM COLOR};
                #math {x} {$j+1};
                #if { "$tokens[$x]" == "[" } {
                    #var {tmp_color} {\e[};
                    #math {x} {$x+1};
                    #while { $x <= $line_width} {

                        #nop end of color symbol;
                        #if { "$tokens[$x]" == "m" } {
                            #cat {tmp_color} {m};
                            #nop showme {> color complete};
                            #break;
                        };

                        #regexp {$tokens[$x]} {{[0-9;]}} {
                            #nop showme {   - color data : '$tokens[$x]'};
                            #cat {tmp_color} {$tokens[$x]};
                        } {
                            #nop showme {> fail : '$tokens[$x]'};
                            #var {tmp_color} {};
                        };
                        #if { "$tmp_color" == "" } {
                            #break;
                        };
                        #math {x} {$x + 1};
                    };
                };
                #unlocal {x};
           
                #if { "$tmp_color" != "" } {
                    #var {last_color} {$tmp_color};
                    #list {color_tokens} {tokenize} {$last_color};
                    #list {color_tokens} {size} {color_size};
#nop showme {$color_tokens};
                    #math {printed_colors_offset} {$printed_colors_offset + $color_size};
                    #math {colored_buf_width} {$buf_width + $printed_colors_offset};
                    #nop showme {Last known color : $color_tokens };
                };
                #unlocal {tmp_color};
            };

            #var {print_line} 0;

            #if { "$tokens[$j]" == " " } {
                #nop showme {WRAP CHECK};
                #math {x} {$j + 1};
                #math {tmp_k} {$k + 1};
                #var {print_line} 1;
                
                #while { $tmp_k <= $colored_buf_width } {
                    #nop local {last_char} {$tokens[$x]};
                    #nop showme {    - '$last_char'};
                    #if { "$tokens[$x]" == " "} {
                        #if { $tmp_k == $colored_buf_width } {
                            #var {print_line} 1;
                        } {
                            #var {print_line} 0;
                        };
                        #break;
                    };
                    #nop END OF MSG REACHED;
                    #if { $x == $line_width } {
                        #var {print_line} 0;
                        #break;
                    };
                    #math {x} {$x + 1};
                    #math {tmp_k} {$tmp_k + 1};

                    #nop TODO changing color on wrap check;
                };

                #nop showme {print_line:$print_line, x:$x, tmp_k:$tmp_k, buf_width:$buf_width, colored_buf_width:{$colored_buf_width}};
                #if { $print_line == 1  && $tmp_k == $colored_buf_width + 1 && "$tokens[$x]" == " "} {
                    #nop showme {ALLOW WORD QUEUE};
                    #var {print_line} 0;
                };
                
                #unlocal {x};

                #if { $print_line == 1 } {
                    #nop showme {TOO LONG};
                    #list {tw_data[%1][tabs][%2][data]} add {$tmp_line};
                    #nop showme {>>> '$tmp_line'};
                    #var {tmp_line} {};
                    #foreach {$last_color} {c} {
                        #cat {tmp_line} {$c};
                    };
                    #var {printed_colors_offset} {$color_size};
                    #math {colored_buf_width} {$buf_width + $printed_colors_offset};
                    #var {k} {$color_size};
                } {
                    #cat {tmp_line} {$tokens[$j]};
                };
            } {
                #cat {tmp_line} {$tokens[$j]};
            };

            #math {j} {$j + 1};
            #math {k} {$k + 1};

            #if { $j == $line_width +1} {
                #list {tw_data[%1][tabs][%2][data]} add {$tmp_line};
            };
        };
        #unlocal {j};
        #unlocal {k};
        #unvar {tmp_line};
        #unvar {print_line};
        #unvar {last_color};
    } {
        #nop showme {>>> '$input_line'};
        #list {tw_data[%1][tabs][%2][data]} add {$input_line};
    };

    #if { {$tw_data[%1][active]} != {%2} } {
        #math {tw_data[%1][tabs][%2][unread]} {$tw_data[%1][tabs][%2][unread] + 1};
    } {
        #var {tw_data[%1][tabs][%2][unread]} 0;
    };

    #unlocal {input_line};
};

#alias tw_switch_tab {
    #var {tw_data[%1][active]} {%2};
    #var {tw_data[%1][tabs][%2][unread]} 0;
};

#alias tw_offset_up {
    #local {active_tab} {$tw_data[%1][active]};
    #math {tw_data[%1][tabs][$active_tab][offset]} {$tw_data[%1][tabs][$active_tab][offset] - %2};
    tw_draw_buffer %1;
    #unlocal {active_tab};
};

#alias tw_offset_down {
    #local {active_tab} {$tw_data[%1][active]};
    #math {tw_data[%1][tabs][$active_tab][offset]} {$tw_data[%1][tabs][$active_tab][offset] + %2};
    #if { $tw_data[%1][tabs][$active_tab][offset] > 0 } {
        #var {tw_data[%1][tabs][$active_tab][offset]} 0;
    };
    tw_draw_buffer %1;
    #unlocal {active_tab};
};

#function {tw_get_buffer} {
    #local {active_tab} {$tw_data[%1][active]};
    #list {tw_data[%1][tabs][$active_tab][data]} {size} {buf_lines};
    #math {buf_start} {(-1 * $tw_data[%1][height]) + $tw_data[%1][tabs][$active_tab][offset]};
    #math {buf_end} {-1 + $tw_data[%1][tabs][$active_tab][offset]};

    #var {buffer_content} {No data yet};

    #if { $buf_lines > 0 } {
        #local {i} {$buf_start};
        #var {buffer_content} {};
        #while { $i <= $buf_end } {
            #if {"$tw_data[%1][tabs][$active_tab][data][$i]" != ""} {
                #cat {buffer_content} $tw_data[%1][tabs][$active_tab][data][$i];
                #cat {buffer_content} {\n};
            };
            #math {i} {$i+1};
        };
    };

    #unlocal {buf_lines};
    #unlocal {buf_start};
    #unlocal {buf_end};
    #unlocal {active_tab};
    #return $buffer_content;
};

#alias tw_draw_tabs {
    #foreach {$tw_data[%1][tabs][1..-1]} {tw_tab} {
        #math {btn_top_row} {$tw_data[%1][top_row]};
        #math {btn_bot_row} {$tw_data[%1][top_row] + $tw_data[%1][tab_height]};
        #math {btn_left_border} {$tw_data[%1][first_col] + ($tw_tab[index]- 1) * $tw_data[%1][tab_size]};
        #math {btn_right_border} {$tw_data[%1][first_col] + ($tw_tab[index]) * $tw_data[%1][tab_size]};
        #var {tab_index} {$tw_tab[index]};

        #if { "%2" == "init_buttons" } {
            #line substitute {VARIABLES} {
                #button {{$btn_top_row} {$btn_left_border} {$btn_bot_row} {$btn_right_border}} {
                    tw_switch_tab %1 $tab_index;
                    tw_draw_tabs %1;
                    tw_draw_buffer %1;
                };
            };
        };
        #elseif { "%2" == "remove_buttons" } {
            #line substitute {VARIABLES} {
                #unbutton {{$btn_top_row} {$btn_left_border} {$btn_bot_row} {$btn_right_border}};
            };
        };
        #else {
            #local {unread_cnt} {};
            #if { $tw_tab[unread] > 0 } {
                #local {unread_cnt} {($tw_tab[unread])};
            };

            #if { $tw_data[%1][active] == $tw_tab[index] } {
                #local {tab_label} {<fda>$tw_tab[index]. $tw_tab[title] ${unread_cnt}};
            } {
                #local {tab_label} {<acf>$tw_tab[index]. $tw_tab[title] ${unread_cnt}};
            };
            #draw azure box {$btn_top_row} {$btn_left_border} {$btn_bot_row} {$btn_right_border} {$tab_label}; 
        };
        #unlocal {tab_label};
    };
};

#alias tw_draw_buffer {
    #local {data} {@tw_get_buffer{%1}};
    #draw azure left right bot top side {$tw_data[%1][top_row]+$tw_data[%1][tab_height]} {$tw_data[%1][first_col]} {$tw_data[%1][end_row]} {$tw_data[%1][end_col]} $data;
    #unlocal {data};
};

